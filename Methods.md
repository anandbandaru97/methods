# Methods

### concat:

1. **Parameter:** It takes n (any) number of values (number, string, boolean, array, null, undefined, object and function etc).

    ``` array1.concat(array2) ```

2. **Return:** A single Array consisting of by all the values passed as parameters in the same order.

3. **Example:**

    ```
    let numbers = [1, 2, 3];
    numbers.concat(4); //[1,2,3,4]

    let sentanceArray = 'A quick brown fox jumped over a lazy'.split(' ');
    sentanceArray.concat('dog').join(' '); 
    //"A quick brown fox jumped over a lazy dog"
    
    let colors = ['red', 'green', 'blue'];
    colors.concat('black', 'red', 21, true); 
    // ['red','green','blue','black', 'red', 21, true]
    ```

4. It accepts n number of values and returns one array with all the values in same order. It does not change the original array.

5. It does not mutate the original array.

### flat:

1. **Parameter:** It takes optional Parameter (By default it is `1` and it takes any integer and Infinity).

    ``` array.flat() ```

2. **Return:** It returns the new flattened array according to the optional parameter given.

3. **Example:**

    ``` 
    var arr1 = [1, 2, [3, 4]];
    console.log(arr1.flat());
    // [1, 2, 3, 4]

    var arr2 = [1, 2, [3, 4, [5, 6]]]; // flattens for depth of 1.
    console.log(arr2.flat());
    // [1, 2, 3, 4, [5, 6]]

    var arr3 = [1, 2, [3, 4, [5, 6]]];
    console.log(arr3.flat(2)); // flattens for depth of 2.
    // [1, 2, 3, 4, 5, 6]

    var arr4 = [1, 2, [3, 4, [5, 6, [7, 8, [9, 10]]]]];
    console.log(arr4.flat(Infinity)); // completely flattens the array.
    // [1, 2, 3, 4, 5, 6, 7, 8, 9, 10] 
    ```

4. It accecpts n number of values and returns the new array in the same order.

5. It does not mutate the original array.

### push:

1. **Parameter:** It takes n (any) number of values (number, string, boolean, array, null, undefined, object and function etc).

    ``` array.push() ```

2. **Return:** It modifies the original array and returns the new length of the array after the values have been added.

3. **Example:**

    ```
    const array1 = [1, 2, 3];
    console.log(array1.push(4)); // return new length of 4.
    console.log(array1); // [1, 2, 3, 4]

    const value = 5;
    array1.push(value);
    console.log(array1); // [ 1, 2, 3, 4, 5 ]

    const array2 = [7, 8, 9];
    array1.push(array2);
    console.log(array1); 
    // [ 1, 2, 3, 4, 5, [ 7, 8, 9 ] ]

    const obj1 = { nums: "10, 11 ,13" };
    array1.push(obj1);
    console.log(array1); 
    // [ 1, 2, 3, 4, 5, [ 7, 8, 9 ], { nums: '10, 11 ,13' } ]

    ```

4. It returns the length of the new array and the mutated array.

5. It mutates the original array.

### indexOf

1. **Parameter:** It takes 2 parameters (one for `searchElement` i.e all data types, and the other `fromIndex` as optional).
    
    ``` array.indexOf(searchElement, fromIndex); ```

2. **Return:** It  is used to search for the first occurrence of a specified value within an array, and it returns the index at which the value was found. If the value is not found in the array, the method returns -1.

3. **Example:**

    ```
    const numbers = [1, 2, 3, 4, 5, 'a', 'b'];

    const index1 = numbers.indexOf(3);
    console.log(index1); // 2

    const index2 = numbers.indexOf(10);
    console.log(index2); // -1

    const index3 = numbers.indexOf(2, 2);
    console.log(index3); // -1 (starts searching from index 2)

    const index4 = numbers.indexOf(4, -2);
    console.log(index4); // 3 (starts searching from index -2, which is equivalent to index 3)

    const index5 = numbers.indexOf(1, 10);
    console.log(index5); // -1 (search start index is beyond the array length)

    console.log(numbers.indexOf('a')); // 5
    ```

4. It returns the index(1st occurrence). If not found `-1` is returned .

5. It does not mutate the original array.

### lastIndexOf

1. **Parameter:** It takes 2 parameters (one for `searchElement` i.e all data types, and the other `fromIndex` as optional).
    
    ``` array.lastIndexOf(searchElement, fromIndex); ```

2. **Return:** It  is used to search for the last occurrence of a specified value within an array, and it returns the index at which the value was found. If the value is not found in the array, the method returns -1.

3. **Example:**

    ```
    const numbers = [1, 2, 3, 4, 5, 3, 6];

    const index1 = numbers.lastIndexOf(3);
    console.log(index1); // 5 (last occurrence of 3)

    const index2 = numbers.lastIndexOf(6);
    console.log(index2); // 6 (last occurrence of 6)

    const index3 = numbers.lastIndexOf(3, 4);
    console.log(index3); // 2 (searching backwards from index 4)

    const index4 = numbers.lastIndexOf(2, -3);
    console.log(index4); // 1 (searching backwards from index -3, which is equivalent to index 4)

    const index5 = numbers.lastIndexOf(1, 10);
    console.log(index5); // 0 (search start index is beyond the array length)
    ```

4. It returns the index(last occurrence). If not found `-1` is returned .

5. It does not mutate the original array.


### includes

1. **Parameter:** It takes 2 parameters (one for `searchElement` i.e all data types, and the other `fromIndex` as optional).

    ``` array.includes(searchElement, fromIndex); ```

2. **Return:** It is used to determine whether an array contains a specific value. It returns a boolean value indicating whether the value is found in the array or not.

3. **Example:**

    ```
    const numbers = [1, 2, 3, 4, 5, 'a', 'b'];

    const includes1 = numbers.includes(3);
    console.log(includes1); // true (3 is found in the array)

    const includes2 = numbers.includes(6);
    console.log(includes2); // false (6 is not found in the array)

    const includes3 = numbers.includes(2, 2);
    console.log(includes3); // false (starts searching from index 2)

    const includes4 = numbers.includes(4, -2);
    console.log(includes4); // true (starts searching from index -2, which is equivalent to index 3)

    console.log(numbers.includes('a')); // true
    ```

4. It returns a `boolean` value.

5. It does not mutate the original array.

### reverse

1. **Parameter:** It does not take any parameters.

    ``` array.reverse() ```

2. **Return:** It modifies the original array and returns the reversed array.

3. **Example:**

    ```
    const originalArray = [1, 2, 3, 4, 5];
    const reversedArray = originalArray.reverse();

    console.log(reversedArray); // [5, 4, 3, 2, 1]
    console.log(originalArray); // [5, 4, 3, 2, 1] (original array is also modified)

    const array1 = ['a', 'b', 'c', 'd'];
    console.log(array1.reverse()); // [ 'd', 'c', 'b', 'a' ]

    const array = [1, 'a', 'b', 2, 4, 5.6];
    console.log(array.reverse()); // [ 5.6, 4, 2, 'b', 'a', 1 ]
    ```

4. It returns the reversed array.

5. It mutates the original array.

### every

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.every(callback(element, index, array), thisArg); ```

2. **Return:** It is used to check if all elements in an array satisfy a specific condition. It returns a boolean value indicating whether every element in the array passes the test implemented by the provided callback function.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [[2, 4, 6, 8, 10], ['apple', 'banana', 'cherry', 'date']];

    const allEven = numbers.every(function (element) {
        return element % 2 === 0;
    });

    const allStartWithA = words.every(word => word.startsWith('a'));

    const allTypes = mixed.every(mix => mix.reverse());

    console.log(allEven); // true (all elements are even)
    console.log(allStartWithA); // false (not all words start with 'a')
    console.log(allTypes) // true (arrays are reversed)
    ```

4. It returns a boolean value.

5. It does not mutate the original array.

### shift

1. **Parameter:** It does not take any parameters.

    ``` array.shift() ```

2. **Return:** It is used to remove the first element from an array and return that removed element. It modifies the original array by removing the element from the beginning.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    const removedNumber = numbers.shift();
    const removedWord = words.shift();
    const removedMix = mixed.shift();

    console.log(removedNumber, numbers); // 2 [ 4, 6, 8, 10 ]
    console.log(removedWord, words); // apple [ 'banana', 'cherry', 'date' ]
    console.log(removedMix, mixed); // 2 [ 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date' ]
    ```

4. It returns the removed element.

5. It mutates the original array.

### splice

1. **Parameter:** It takes n (any) number of values (number, string, boolean, array, null, undefined, object and function etc).
First 2 parameters are numbers.

    ``` array.splice(startIndex, deleteCount, item1, item2, ...); ```

2. **Return:** It is used to change the contents of an array by removing or replacing existing elements or adding new elements. It modifies the original array and returns an array containing the removed elements(if any).

3. **Example:**

    ```
    const numbers = [1, 2, 3, 4, 5];
    const removedElements = numbers.splice(1, 2); // Removing elements at index 1 and 2
    numbers.splice(0, 1, 'apple', 'banana'); // Replacing elements at index 0 with 'apple' and 'banana'
    numbers.splice(2, 0, 'cherry', 'date'); // Adding elements at index 2

    console.log(removedElements); //  [2, 3]
    console.log(numbers); // [1, 4, 5]
    console.log(numbers); // ['apple', 'banana', 4, 5]
    console.log(numbers); // Output: ['apple', 'banana', 'cherry', 'date', 4, 5]
    ```

4. It modifies the original array and returns an array containing the removed elements(if any).

5. It mutates the original array.

### find

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.find(callback(element, index, array), thisArg); ```

2. **Return:** It is used to search through an array and retrieve the first element that satisfies a given condition. It returns the value of the first element that matches the condition, or undefined if no such element is found.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    const number = numbers.find(function (num) {
        return num === 2;
    });
    const word = words.find(function(str){
        return str === 'date';
    });
    const mix = mixed.find(function(str){
        if(str === 12){
            return str;
        } else if(str === 'word'){
            return str;
        }
    });

    console.log(number); // 2
    console.log(word); // date
    console.log(mix); // undefined
    ```

4. It returns found element else undefined.

5. It does not mutate the original array.

### unshift

1. **Parameter:** It takes all data types.

    ``` array.unshift() ```

2. **Return:** It is used to add one or more elements to the beginning of an array. It modifies the original array and returns the new length of the array after the elements have been added.

3. **Example:**

    ```
    const numbers = [4, 5, 6];

    const newLength = numbers.unshift(1, 2, 3);
    console.log(newLength); // 6 (new length of the array)
    console.log(numbers); // [1, 2, 3, 4, 5, 6] (original array modified)

    const newLen = numbers.unshift([7, 8, 9]);
    console.log(newLen); // 7
    console.log(numbers); // // [ [ 7, 8, 9 ], 1, 2, 3, 4, 5, 6 ]

    const newObj = numbers.unshift({ name: 'Devil' });
    console.log(newObj); // 8
    console.log(numbers); // [ { name: 'Devil' }, [ 7, 8, 9 ], 1, 2, 3, 4, 5, 6 ]
    ```

4. It returns the new length of the array and the modifies the original array.

5. It mutates the original array.


### findIndex

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.findIndex(callback(element, index, array), thisArg); ```

2. **Return:** It is used to search through an array and retrieve the index of the first element that satisfies a given condition. It returns the index of the first element that matches the condition, or -1 if no such element is found.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    const number = numbers.findIndex(function (num) {
        return num === 2;
    });
    const word = words.findIndex(function(str){
        return str === 'date';
    });
    const mix = mixed.findIndex(function(str){
        if(str === 12){
            return str;
        } else if(str === 'word'){
            return str;
        }
    });

    console.log(number); // 0
    console.log(word); // 3
    console.log(mix); // -1
    ```
4. It returns found element index else -1(not found).

5. It does not mutate the original array.
   
### filter

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.filter(callback(element, index, array), thisArg); ```

2. **Return:** It is used to create a new array containing all elements that satisfy a given condition. It iterates over each element of the array and applies a provided callback function, returning a new array with elements that pass the condition.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    const number = numbers.filter(function (num) {
        return num % 2 === 0;
    });
    const word = words.filter(function (str) {
        return typeof (str) === 'string';
    });
    const mix = mixed.filter(function (str) {
        if (str % 4 === 0 || typeof (str) === 'string') {
            return str;
        }
    });

    console.log(number); // [ 2, 4, 6, 8, 10 ]
    console.log(word); // [ 'apple', 'banana', 'cherry', 'date' ]
    console.log(mix); // [ 4, 8, 'apple', 'banana', 'cherry', 'date' ]

    ```

4. It returns new filtered array without any mutations.

5. It does not mutate the original array.

### forEach

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.forEach(callback(element, index, array), thisArg); ```

2. **Return:** It is used to iterate over the elements of an array and execute a provided callback function for each element. It doesn't create a new array. Instead, it's used for performing operations or actions on each element in the existing array.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple banana cherry date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    numbers.forEach(function (number) {
        console.log(number);
    });
    // 2
    // 4
    // 6
    // 8
    // 10

    words.forEach(function (word) {
        console.log(word);
    });
    // apple banana cherry date

    mixed.forEach(function (mix) {
        console.log(mix);
    });
    // 2
    // 4
    // 6
    // 8
    // 10
    // apple
    // banana
    // cherry
    // date
    ```
    
4. It returns each iteration on log.

5. It does not mutate the original array.
   
### map

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.filter(callback(element, index, array), thisArg); ```

2. **Return:** It is used to create a new array by applying a provided callback function to each element of an existing array. It returns a new array with the results of applying the callback function to each element.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [2, 4, 6, 8, 10, 'apple', 'banana', 'cherry', 'date'];

    const squaredNumbers = numbers.map(function (num) {
        return num ** 2;
    });
    const addedWords = words.map(function (word) {
        return `fruit: ${word}` ;
    });
    const mixedNumStr = mixed.map(function (str) {
        if (str % 2 === 0) {
            return str;
        } else {
            return -1;
        }
    });

    console.log(squaredNumbers); // [ 4, 16, 36, 64, 100 ]
    console.log(addedWords); // [ 'fruit: apple', 'fruit: banana', 'fruit: cherry', 'fruit: date' ]
    console.log(mixedNumStr); // [2, 4, 6, 8, 10, -1, -1, -1, -1]
    ```

4. It returns new mapped array without any mutations.

5. It does not mutate the original array.

### pop

1. **Parameter:** It does not take any parameters.

    ``` array.pop() ```

2. **Return:** It is used to remove the last element from the end of an array and return that removed element. It modifies the original array and returns the removed element.

3. **Example:**

    ```
    const array1 = [1, 2, 3];

    console.log(array1.pop()); // return length of 3.
    console.log(array1); // [1, 2]

    const fruits = ['apple', 'banana', 'cherry'];

    const removedFruit = fruits.pop();
    console.log(removedFruit); // 'cherry'
    console.log(fruits); // ['apple', 'banana'] (original array modified)

    ```

4. It returns the last element removed and if the array is empty it returns undefined.

5. It mutates the original array.

### reduce

1. **Parameter:** It takes callback function and one optional parameter.

    ``` const result = array.reduce(callback(accumulator, currentValue, index, array), initialValue); ```

2. **Return:** It is used to accumulate or reduce the elements of an array into a single value. It applies a provided callback function to each element in the array, sequentially combining the results to produce a single output.

3. **Example:**

    ```
    const numbers = [1, 2, 3, 4, 5];
    const prices = [10.99, 5.99, 3.49, 6.87];
    const words = ['An', 'an', 'd'];

    const sum = numbers.reduce(function (accumulator, currentValue) {
        return accumulator + currentValue;
    }, 0);
    const totalPrice = prices.reduce((accumulator, currentValue) => accumulator + currentValue);
    const name = words.reduce((accumulator, currentValue) => accumulator + currentValue);

    console.log(sum); // 15 (1 + 2 + 3 + 4 + 5)
    console.log(totalPrice); // 27.34 (10.99 + 5.99 + 3.49 + 6.87)
    console.log(name); // Anand (An + an + d)
    ```

4. It returns a single value without any mutations.

5. It does not mutate the original array.

### slice

1. **Parameter:** It takes only 2 parameters (startIndex, endIndex(optional)).

    ``` array.slice(startIndex, endIndex); ```

2. **Return:** It is used to create a shallow copy of a portion of an array into a new array. It does not modify the original array but instead returns a new array containing the selected elements.

3. **Example:** 

    ```
    const numbers = [1, 2, 3, 4, 5];

    const slicedNumbers = numbers.slice(1, 4);

    console.log(slicedNumbers); // Output: [2, 3, 4] (elements from index 1 to 3)
    console.log(numbers); // Output: [1, 2, 3, 4, 5] (original array remains unchanged)

    const originalArray = [1, 2, 3, 4, 5];
    const newArray = originalArray.slice();

    console.log(newArray); // Output: [1, 2, 3, 4, 5] (copy of the original array)

    const array1 = [1, 2, 3];

    console.log(array1.slice(1)); // [2, 3]
    console.log(array1); // [ 1, 2, 3 ]
    ```

4. It returns the sliced array.

5. It does not mutate the original array.


### some

1. **Parameter:** It takes callback function and one optional parameter.

    ``` array.some(callback(element, index, array), thisArg); ```

2. **Return:** It is used to check if at least one element in an array satisfies a given condition. It returns a boolean value indicating whether any element in the array passes the test implemented by the provided callback function.

3. **Example:**

    ```
    const numbers = [2, 4, 6, 8, 10];
    const words = ['apple', 'banana', 'cherry', 'date'];
    const mixed = [[2, 4, 6, 8, 10], ['apple', 'banana', 'cherry', 'date']];

    const allEven = numbers.some(function (element) {
        return element % 2 === 0;
    });

    const allStartWithA = words.some(word => word.startsWith('a'));

    const allTypes = mixed.some(mix => mix.reverse());

    console.log(allEven); // true (some elements are even)
    console.log(allStartWithA); // true (some words start with 'a')
    console.log(allTypes) // true (arrays are reversed)
    ```
    
4. It returns a boolean value.

5. It does not mutate the original array.
